import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.*;


public class KWIC {
    //Find ignore words;
    public static List<List<String>> getIgnoredAndTitles(String input) {
        List<List<String>> res = new ArrayList<>();
        List<String> ignored = new ArrayList<>();
        List<String> titles = new ArrayList<>();
        Scanner sc = new Scanner(input);
        while (sc.hasNext()) {
            String str = sc.next();
            if (str.equals("::")) {
                break;
            }
            ignored.add(str);
        }
        res.add(ignored);
        String empty = sc.nextLine();
        while (sc.hasNextLine()){
            String str = sc.nextLine();
            titles.add(str);
        }
        res.add(titles);
        return res;
    }

    //Creat Map, store the key words in each
    public static List<String> getKWIC(List<List<String>> ignoredAndTitles) {
        List<String> ignored = ignoredAndTitles.get(0);
        List<String> titles = ignoredAndTitles.get(1);
        List<List<String>> ans = new ArrayList<>();
        for (String title : titles) {
            List<String> splitTitleWords = Arrays.asList(title.toLowerCase().split(" "));
            List<Integer> idx = new ArrayList<>();
            for(int i = 0; i < splitTitleWords.size(); i++){
                if(!ignored.contains(splitTitleWords.get(i))){
                    idx.add(i);
                }
            }
            System.out.println(idx);
            for(int i : idx){
                String tmp = splitTitleWords.get(i);
                splitTitleWords.set(i, tmp.toUpperCase());
                StringBuilder sb = new StringBuilder();
                for (String word : splitTitleWords) {
                    sb.append(word+' ');
                }
                List<String> tmpList = new ArrayList<>();
                tmpList.add(tmp.toUpperCase());
                tmpList.add(sb.toString().trim());
                ans.add(tmpList);
//         System.out.println(sb.toString());
                splitTitleWords.set(i, tmp);
            }
        }
        Collections.sort(ans, new Comparator<List<String>>() {
            @Override
            public int compare(List<String> o1, List<String> o2) {
                return o1.get(0).compareTo(o2.get(0));
            }
        });


        List<String> res = new ArrayList<>();
        for(int i = 0; i < ans.size(); i++){
            res.add(ans.get(i).get(1));
        }

        return res;
    }

    public static void main(String[] args) {
        String input = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG";
        List<String> res = getKWIC(getIgnoredAndTitles(input));

        for (String str : res) {
            System.out.println(str);
        }
    }


}