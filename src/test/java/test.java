import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Disabled;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import java.util.List;

class KWICTest {

    @Test
    @DisplayName("Test the ignored and titles")
    void Test_TitlesandIgnored() {
        String input = "is\n" +
                "the\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n";
        List<String> title = new ArrayList<>();
        List<String> ignored = new ArrayList<>();
        List<List<String>> expected = new ArrayList<>();
        ignored.add("is");
        ignored.add("the");
        title.add("Descent of Man");
        title.add("The Ascent of Man");
        expected.add(ignored);
        expected.add(title);
        assertEquals(new KWIC().getIgnoredAndTitles(input), expected);
    }

    @Test
    @DisplayName("Test the keywords")
    void Test_Keywords() {
        String input = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        String expected = """
                a portrait of the ARTIST as a young man
                the ASCENT of man
                a man is a man but BUBBLESORT is a dog
                DESCENT of man
                a man is a man but bubblesort is a DOG
                descent of MAN
                the ascent of MAN
                the old MAN and the sea
                a portrait of the artist as a young MAN
                a MAN is a man but bubblesort is a dog
                a man is a MAN but bubblesort is a dog
                the OLD man and the sea
                a PORTRAIT of the artist as a young man
                the old man and the SEA
                a portrait of the artist as a YOUNG man
                """;
        List<String> res = new KWIC().getKWIC(new KWIC().getIgnoredAndTitles(input));
        String str = "";
        for (String str_2: res){
            str+=str_2;
            str+="\n";
        }
        str = str.substring(0, str.length() - 1);
        expected = expected.substring(0,expected.length() - 1);
        assertEquals(expected, str);

    }
}
